package coid.bca.repository;

import org.springframework.stereotype.Repository;

@Repository
public class AuditTrailRepository {

	public void save(String action) {
		System.out.println("Save Audit Trail. Action: " + action);
	}
	
}
