package coid.bca.repository;

import java.lang.annotation.Annotation;

import org.springframework.stereotype.Repository;



@Repository
public class BankRepository implements MyRepository {

	public void save() {
		System.out.println("Save Bank.");
	}
	
	public void edit() {
		System.out.println("Edit Bank.");
	}
	
	public void delete() {
		System.out.println("Delete Bank.");
	}
	
}
