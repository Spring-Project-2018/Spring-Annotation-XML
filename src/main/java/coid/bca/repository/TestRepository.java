package coid.bca.repository;

import java.lang.annotation.Annotation;

import org.springframework.stereotype.Repository;



@Repository
public class TestRepository implements MyRepository {

	public void save() {
		System.out.println("Save Test.");
	}
	
	public void edit() {
		System.out.println("Edit Test.");
	}
	
	public void delete() {
		System.out.println("Delete Test.");
	}
	
}
