package coid.bca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import coid.bca.repository.AuditTrailRepository;
import coid.bca.repository.BranchRepository;
import coid.bca.repository.MyRepository;

@Service("CabangService")
public class BranchService {
	@Autowired
	private AuditTrailRepository auditTrailRepository;
	@Autowired
	@Qualifier("branchRepository")
	private MyRepository repository;
	
	public void save() {
		auditTrailRepository.save("Save Branch");
		repository.save();
	}
	
	public void edit() {
		auditTrailRepository.save("Edit Branch");
		repository.edit();
	}
	
	public void delete() {
		auditTrailRepository.save("Delete Branch");
		repository.delete();
	}
	
	public void setAuditTrailRepository(AuditTrailRepository auditTrailRepository) {
		this.auditTrailRepository = auditTrailRepository;
	}
	
	public void setBranchRepository(BranchRepository branchRepository) {
		this.repository = branchRepository;
	}
	
}
